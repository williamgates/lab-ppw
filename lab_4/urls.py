from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('contact/', views.contact, name='contact'),
    path('guest-book/', views.guest_book, name='guest-book'),
    path('projects/', views.projects, name='projects'),
    path('track-record/', views.track_record, name='track-record'),
    path('add-schedule/', views.add_schedule, name='add-schedule'),
    path('process-schedule/', views.process_schedule, name='process-schedule'),
    path('schedule-list/', views.schedule_list, name='schedule-list'),
    path('delete-all-schedule/', views.delete_all_schedule, name='delete-all-schedule'),
]
