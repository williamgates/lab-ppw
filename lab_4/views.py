from datetime import datetime
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .models import Schedule

def index(request):
    return render(request, 'lab_4/index.html', {})

def contact(request):
    return render(request, 'lab_4/contact.html', {})

def guest_book(request):
    return render(request, 'lab_4/guest-book.html', {})

def projects(request):
    return render(request, 'lab_4/projects.html', {})

def track_record(request):
    return render(request, 'lab_4/track-record.html', {})

def add_schedule(request):
    return render(request, 'lab_4/add-schedule.html', {})

def process_schedule(request):
    try:
        date = request.POST['date'].split("/")
        date.reverse()
        date = "-".join(date)
        date = datetime.strptime(date, "%Y-%m-%d").date()

        day_list = {'Sunday': 'Minggu', 'Monday': 'Senin', 'Tuesday': 'Selasa', 'Wednesday': 'Rabu', 'Thursday': 'Kamis', 'Friday': 'Jumat', 'Saturday': 'Sabtu'}
        day = day_list[date.strftime("%A")]

        time = request.POST['time'].split(".")
        time = ":".join(time + ["00"])
        time = datetime.strptime(time, '%H:%M:%S')

        schedule = Schedule(day=day,
                            date=date,
                            time=time,
                            activity_name=request.POST['activity-name'],
                            place=request.POST['place'],
                            category=request.POST['category'])
        schedule.save()
    except:
        return render(request, 'lab_4/add-schedule.html', {
            'message': "Terdapat input yang salah!"
        })

    return render(request, 'lab_4/add-schedule.html', {
        'message': "Penambahan jadwal berhasil!"
    })

def schedule_list(request):
    schedules = Schedule.objects.all()
    context = {'schedules': schedules}
    return render(request, 'lab_4/schedule-list.html', context)

def delete_all_schedule(request):
    try:
        if (request.method == 'POST' and request.POST['command'] == 'Delete'):
            Schedule.objects.all().delete()
        else:
            raise Exception()
    except:
        return render(request, 'lab_4/schedule-list.html', {
            'message': 'Error!'
        })

    return render(request, 'lab_4/schedule-list.html', {
        'message': 'Semua jadwal terhapus!'
    })
