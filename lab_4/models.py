from django.db import models

class Schedule(models.Model):
    day = models.CharField(max_length=10)
    date = models.DateTimeField()
    time = models.TimeField()
    activity_name = models.CharField(max_length=30)
    place = models.CharField(max_length=30)
    category = models.CharField(max_length=30)

    def __str__(self):
        return self.activity_name
